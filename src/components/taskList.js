import React from 'react';
import { Link } from "gatsby"
import axios from 'axios';

export default class TaskList extends React.Component {
  state = {
    tasks: []
  }

  componentDidMount() {
    axios.get(`http://localhost/wordpress/wp-json/Taskmanager/v0/task-post`)
      .then(res => {
        const tasks = res.data;
        this.setState({ tasks });
      })
  }

  render() {
    return (
        
      <div className='container'>
        <h2>Tâche</h2>
        <div className="row">
          { this.state.tasks.map(task =>
            <div className='card col-6'>
              <p>Titre : {task.post_title}</p>
              <p>Description de la Tâche : {task.post_content}</p>
              <p>Date de fin : {task.post_date_gmt}</p>
              <p>Responsable : {task.post_author}</p>
              <p>État de la Tâche : {task.post_status}</p> <br />
              <Link to='/task_single/' state={{ 
                id: task.ID, 
                titre: task.post_title,
                text: task.post_content, 
                date: task.post_date_gmt, 
                responsable: task.post_author,
                status: task.post_status }}>Voir la Tâche</Link> <br />
            </div>
            
          )}
        </div> 
      </div>
    )
  }
}