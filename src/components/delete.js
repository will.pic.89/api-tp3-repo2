import React from "react"
import Header from "../components/header"
import axios from 'axios';

export default class TaskList extends React.Component {
  state = {
    id: '',
    location
  }

  handleChange = event => {
    this.setState({ id: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    axios.delete(`http://localhost/wordpress/wp-json/Taskmanager/v0/task-post/${location.state.id}`)
      .then(res => {
        const task = res.data;
        this.setState({ task });
      })
  }

  render() {
    return (
      <div>
        <Header />
        <div className='container'>
          <form onSubmit={this.handleSubmit}>
            {location.state.id}
            <h2>Tâche</h2><br />
            <label for="titre">Titre</label><br />
            <input type="text" name="titre" id="titre" value={location.state.titre} /><br /><br />
            <label for="description">Description de la Tâche</label><br />
            <input type="text" name="description" id="description" value={location.state.text} /><br /><br />
            <label for="date">Date de fin</label><br />
            <input type="text" name="date" id="date" value={location.state.date} /><br /><br />
            <label for="responsable">Responsable</label><br />
            <input type="text" name="responsable" id="responsable" value={location.state.responsable} /><br /><br />
            <label for="status">État de la Tâche</label><br />
            <input type="text" name="status" id="status" value={location.state.status} /><br />
            <button type="submit">Delete</button>
          </form>
        </div>
      </div>
    )
  }
}