import React from "react"
import { Link } from "gatsby"


export default () => 

<div className="pt-4 pl-4 pb-4 container">
  <nav className="nav">
    <Link className="nav-link active" to="/">Accueil</Link>
    <Link className="nav-link" to="/outil">Présentation</Link>
    <Link className="nav-link" to="/documentation">Documentation</Link>
    <Link className="nav-link" to="/task">Gestionnaire de tâche</Link>
    <Link className="nav-link" to="/contact">Contact</Link>
  </nav>
</div>
    
