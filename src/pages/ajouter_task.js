import React from 'react';
import Header from "../components/header"
import axios from 'axios';


export default class TaskList extends React.Component {
    state = {
        ID:'', 
        title: '',
        content: '',
        date_gmt: '',
        author: '',
    }

    handleChange = event => {
        this.setState({ ID: event.target.value, title: event.target.value, content: event.target.value, date_gmt: event.target.value, author: event.target.value });
      }
    
      handleSubmit = event => {
        event.preventDefault();
    
        const user = {
            ID: this.state.ID,
            title: this.state.title,
            content: this.state.content,
            date_gmt: this.state.date_gmt,
            author: this.state.author
        };

        axios.post(`http://localhost/tp2-soucy-calderon-picard/wordpress/wp-json/Taskmanager/v0/task-post`, {user})
        .then(res => {
            console.log(res);
            console.log(res.data);
        })
    }

    render() {
        return (
            <div>
                <Header />
                <div className="container">
                    <form onSubmit={this.handleSubmit}>
                        <h2>Tâche</h2><br />
                        <label htmlFor="titre">Titre</label><br />
                        <input type="text" name="titre" id="titre" placeholder="Titre Ici" onChange={this.handleChange} /><br /><br />
                        <label htmlFor="description">Description de la Tâche</label><br />
                        <input type="text" name="description" id="description" placeholder="Description Ici" onChange={this.handleChange}/><br /><br />
                        <label htmlFor="date">Date de fin</label><br />
                        <input type="date" name="date" id="date" onChange={this.handleChange}/><br /><br />
                        <label htmlFor="responsable">Responsable</label><br />
                        <input type="text" name="responsable" id="responsable" placeholder="responsable Ici" onChange={this.handleChange}/><br /><br />
                        <label htmlFor="status">État de la Tâche</label><br />
                        <input type="text" name="status" id="status" placeholder="état de la Tâche Ici" onChange={this.handleChange}/><br /><br />
                        <button type="submit">Ajouter la Tâche</button>
                    </form>
                </div>
            </div>
        )
    }
}