import React from "react"
import Header from "../components/header"


export default () =>
<div>
<Header /> 
  <main className="container">
    <div>
      <h1 className="text-center text-uppercase mb-3">Task'n Go</h1>
      <p>
      Merci d'utiliser Task'n Go le meilleur plugin de taskmanager. Produit d'excellence, approuvé par le consommateur et choix #1 de ceux çi, vous savez que vous ne pouvez pas vous tromper. Avec Task'n Go vous pouvez créé des tache qui seront vu par tout vos employer, vous allez pouvoir selectionner une personne responsable. Par ailleur, vos employer vont pouvoir commenter les tâches et même changer le statue de celle-ci.
      </p>
      <img src="https://static.vecteezy.com/system/resources/previews/000/163/725/non_2x/content-creator-vector.png" className="rounded mx-auto d-block" alt="Responsive" width="600" height="auto" />
    </div>
  </main>
</div>
