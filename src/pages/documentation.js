import React from "react"
import Header from "../components/header"

export default () =>
<div>
    <Header />
    <main className="container">
        <div>
            <p>
                Ce plugin est créé pour faciliter la répartion des tâches dans votre entreprise. Vous allez pouvoir créer des tâches, les modifier, les commenter ou même les supprimer à votre guise.

                <br /><br />Création d'une tâche en détail:
            </p>
            
            <ol>
                <li>Activer le plugin dans les extensions installer.</li>
                <li>Aller dans l'onglet Gestionnaire de tâche.</li>
                <li>Faite un click sur le bouton ajouter.</li>
                <li>Ensuite il faut remplir les cases vide et mettre un titre.</li>
                <li>Pour fini il faut simplement faire un click sur le bouton publier.</li>
            </ol>

            <p>Et voilà vous venez votre première tâche en utilisant notre plugin!!!</p>
        </div>
    </main>
</div>