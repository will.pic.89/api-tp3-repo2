import React from "react"
import Header from "../components/header"
import TaskList from "../components/taskList"
import { Link } from "gatsby"

export default () =>
<div>
<Header />
<TaskList />
<div className='container pt-4'><Link to="/ajouter_task">Ajouter une tâche</Link></div>
</div>
