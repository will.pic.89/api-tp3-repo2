import React from "react"
import Header from "../components/header"

export default () =>
<div>
    <Header />
    <main className="container">
        <div>
            <h1>Présentation de l'outil</h1>
            <h3>Introduction</h3>
            <p>Nous avons donc décidé de vous donner un plugin avec Gatsby. Techniquement, il est composé d’une simple liste de fichiers js, qui affiche les mêmes informations quel que soit le visiteur. 
            Il suffit d’uploader les fichiers sur un service de stockage et le tour est joué.
            Les deux principaux avantages d’un site statique sont la sécurité et la vitesse. 
            La plupart du temps, le contenu est géré via des fichiers a une API de contenu.
            Ensuite, le générateur requête le contenu, l’injecte dans les templates définis par le développeur et génère un ensemble de fichiers HTML, CSS et JavaScript.</p>
        </div>
        <div>
            <h1>Présentation de l'équipe</h1>
            <div className="row">
                <div className="col-4">
                    <div className="card">
                        <img src="https://i.pravatar.cc/300?img=3" alt="William-Picard" className="card-img-top" />
                        
                        <div className="card-body">
                            <h2 className="card-title">William Picard</h2>
                            <p className="card-text">L'<strong>analyste fonctionnel</strong> Puisqu'il n'a pas le temps de réaliser le système, un second informaticien vient l'aider pour programmer, il s'agit d'un programmeur.</p>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="card">
                        <img src="https://i.pravatar.cc/300?img=4" alt="Sebastien-Soucy" className="card-img-top" />
                        
                        <div className="card-body">
                            <h2 className="card-title">Sébastien Soucy</h2>
                            <p className="card-text"><strong>Chargé de projet</strong> s'ajoute aussi à l'équipe. Son rôle est d'encadrer l'équipe de développement afin de s'assurer de respecter les échéanciers et les budgets.</p>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="card">
                        <img src="https://i.pravatar.cc/300?img=5" alt="Maria-Calderon" className="card-img-top" />
                        
                        <div className="card-body">
                            <h2 className="card-title">Maria Calderon</h2>                        
                            <p className="card-text">L'<strong>intégrateur</strong> veille à ce que l'arrimage entre les composantes logicielles et/ou systémiques fonctionne bien.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

//source:http://www.fonctionnel.net/profession/acteurs-projet-informatique
//source:https://jamstatic.fr/2018/04/26/construire-un-blog-statique-avec-gatsby-et-strapi/
//source:http://www.adaltas.com/fr/adn/2018-adaltas-summit/react-et-graphql-avec-gatsby-js-plus-quun-generateur-de-site/
