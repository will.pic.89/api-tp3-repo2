import React from "react"



export default () =>
<div className='container row d-flex justify-content-center w-100 vh-100'>
    <div className="text-center col-md-6">
        <h1>404</h1>
        <h2>THE PAGE YOU REQUESTED WAS NOT FOUND</h2>
    </div>
</div>

