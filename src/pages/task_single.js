import React from "react"
import Header from "../components/header"
import axios from 'axios';

export default ({ location}) =>
<div class='container'>

<Header />
  <form action="/action_page.php">
    {location.state.id}
    <h2>Tâche</h2><br />
    <label for="titre">Titre</label><br />
    <input type="text" name="titre" id="titre" value={location.state.titre} /><br /><br />
    <label for="description">Description de la Tâche</label><br />
    <input type="text" name="description" id="description" value={location.state.text} /><br /><br />
    <label for="date">Date de fin</label><br />
    <input type="text" name="date" id="date" value={location.state.date} /><br /><br />
    <label for="responsable">Responsable</label><br />
    <input type="text" name="responsable" id="responsable" value={location.state.responsable} /><br /><br />
    <label for="status">État de la Tâche</label><br />
    <input type="text" name="status" id="status" value={location.state.status} /><br />
  </form>
</div>


/*
export default class PersonList extends React.Component {
  state = {
    name: '',
  }

  handleChange = event => {
    this.setState({ name: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();

    const user = {
      name: this.state.name
    };

    axios.post(`https://jsonplaceholder.typicode.com/users`, { user })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  render(location) {
    return (
<div class='container'>

<Header />
  <form action="/action_page.php">
    {location.state.id}
    <h2>Tâche</h2><br />
    <label for="titre">Titre</label><br />
    <input type="text" name="titre" id="titre" value={location.state.titre} /><br /><br />
    <label for="description">Description de la Tâche</label><br />
    <input type="text" name="description" id="description" value={location.state.text} /><br /><br />
    <label for="date">Date de fin</label><br />
    <input type="date" name="date" id="date" value={location.state.date} /><br /><br />
    <label for="responsable">Responsable</label><br />
    <input type="text" name="responsable" id="responsable" value={location.state.responsable} /><br /><br />
    <label for="status">État de la Tâche</label><br />
    <input type="text" name="status" id="status" value={location.state.status} /><br />
    <button type="submit">Modifier</button>
  </form>
</div>
)
}
} */
