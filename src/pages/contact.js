import React from "react"
import Header from "../components/header"

export default () =>
<div>
  <Header /> 
  <main className="container">
    <div className="row">
        <div className="col">
            <div className="card mb-0">
                <h3 className="text-center"><i className="far fa-edit"></i> CONTACTEZ NOUS</h3>
                <form action="https://formspree.io/mafecalderon@hotmail.com" method="POST">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="card-body">
                                <div className="row mb-3">
                                    <div className="col">
                                        <div className="input-group"><span className="input-group-addon"><i
                                                    className="fa fa-user-circle"></i></span>
                                            <input className="form-control" type="text" name="name" placeholder="Nom"
                                                required="required" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col">
                                        <div className="input-group"><span className="input-group-addon"><i
                                                    className="fa fa-file-text"></i></span>
                                            <input className="form-control" type="text" name="Subject" placeholder="Subject"
                                                required="required" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-3">
                                    <div className="col">
                                        <div className="input-group"><span className="input-group-addon"><i
                                                    className="fa fa-envelope"></i></span>
                                            <input className="form-control" type="email" name="_replyto"
                                                placeholder="E-mail" required="required" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                          <div className="card-body">
                            <div className="form-group">
                                <textarea className="form-control" name="message" placeholder="Message"
                                    required="required"></textarea>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <button className="btn btn-lime" type="submit">Envoyer</button>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
</div>
